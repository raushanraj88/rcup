import sys, os
sys.path.append(os.path.abspath('..'))

import pytest
from falcon import testing
from app.falcon_config import FalconConfig
from app.utils.db_utility import FTestingDB
from app.tests.db import db

db_ops = FTestingDB()
#db_ops.drop_db()
#db_ops.create_db()
config  = FalconConfig(db)
app = config.create_app()

@pytest.fixture(scope="module")
def client():
    return testing.TestClient(app)





