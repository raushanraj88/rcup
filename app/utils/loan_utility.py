import falcon
from app.loan.models import  LoanComponent, Loan


def loan_exist(req, resp, resource, params):
    loan_id = params.get("loan_id")
    if loan_id:
        try:
            loan = Loan.get(Loan.id == loan_id)
            req.context["loan"]=loan
        except Loan.DoesNotExist as e:
            raise falcon.HTTPInvalidParam("","loan_id")