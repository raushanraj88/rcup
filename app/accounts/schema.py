import  uuid
from marshmallow import Schema, fields, validate, post_load
from app.accounts.models import User

class UserSchema(Schema):
    name = fields.Str(required=False,validate=[validate.Length(max=255)])
    username = fields.Str(required=True,validate=[validate.Regexp(regex=r'^[\w.@+-]+$',
                          error='Username is not valid. Numbers,alphabets,+,_,-,. are allowed.'),validate.Length(min=3,max=255)])
    password = fields.Str(required=True, load_only=True, validate = [validate.Regexp(regex=r'[A-Za-z0-9@#$%^&+=]{8,}',
                          error="Enter strong password with special characters"),validate.Length(min=8,
                          error="Minimum 8 characters required for password")])
    email = fields.Email(required=True,validate=[validate.Email(error='Not a valid email address'),validate.Length(max=255)])

    secret = fields.UUID(missing=uuid.uuid4)

    is_active = fields.Boolean(required=False)

    @post_load
    def password_hash(self,data):
        data["password"] = User.hash_password(data["password"])
        return data

