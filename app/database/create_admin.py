import sys, os
sys.path.append(os.path.abspath('..'))

from app.accounts.models import User, Role

role = Role.get(Role.id == Role.ADMIN)


if __name__  == "__main__":
    usernames = sys.argv[1:]
    for username in usernames:
        user = User.get(User.username == username )
        user.role = role
        user.save()


