import falcon

from app.middlewares.db_middleware import PeeweeConnectionMiddleware
from app.middlewares.generic_middleware import RequireJSON
from app.accounts.routes import urls as account_urls
from app.loan.routes import urls as loan_urls

class FalconConfig(object):

    def __init__(self,db):
        self.app = None
        self.db = db

    def create_app(self):
        self.initialize()

    def get_middlewares(self):
        return [PeeweeConnectionMiddleware(self.db),RequireJSON()]

    def all_urls(self):
        return account_urls+loan_urls

    def initialize(self):
        self.app = falcon.API(middleware = self.get_middlewares())
        for (args,kwargs) in self.all_urls():
            self.app.add_route(*args,**kwargs)
