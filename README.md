# RCUP LOAN MANAGEMENT SYSTEM

**CORE INFRATRUCTURE**

* Backend
	* Language - Python
	* Application Server - Falcon, Gunicorn
	* Database & ORM - Postgresql, Peewee
	* Validator - Marshmallow 
* Docker
	* Commands: `sudo docker build -t app .`  `sudo docker-compose build`  `sudo docker-compose up -d`
	* Running db operations: ` sudo docker-compose exec app /bin/sh -c "cd app && python database/db_ops.py" `
	* API server is accesible to 127.0.0.1:5000

**FEATURES**

* Users, Roles and Authentication
	* There are two table User and Role with foreign relation between them. 
	* User has an additional uuid4 secret field.
	* **/register** will simply register any one with Role.CUSTOMER
	* **/login** and **/register** will return jwt token
	* jwt token's has one project wide secret and user specific public secret encrypted.
	* Validation of jwt is done on every request with help of pre hook on views **Authorize([<list_of_roles>])**
	* User's uuid is in public payload and is checked during authenticaion on backend. payload = {"user_data"& "user_uuid"}
	* Incase of invalidating the token simply changing uuid will work, also in case global secret is compromised, it won't affect the tokens secrecy.
	* `python database/create_admin <list_of_registered_user>` will create admin account.
	* Admin can create agent on  post with user_id `/user/agent/create`
	* Listing viewing and Editing has proper permission checked enabled
	* There are two separate modules i.e; **accounts** and **users** in project directory.
	* Future models like personal and final info is also included with flat structure

* Loan
	* Loan has two component one is LoanMeta (Class Loan)  and other is LoanComponent (Class LoanComponent).
	* First time creation of loan will creat loan(user, approved_amount, etc) and editable loancomponent(req. amount, interest rate, duration, etc) objects in db.
	* Consecutive edits for loan will save new loancomponent object with is_discarded field (for rollback purpose).
	* DetailView of loan is loanmeta + recent loancomponent and not discarded.
	* History Edits are all loancomponent object other than recently created.
	* Emi calculation occured on recent loancomponent (Both flat and reducing).
	* Authorized Listing with filter can be done by any roles.
	
**TESTCASES**
# TODO

` sudo docker-compose exec app /bin/sh -c "cd app && pytest tests"`

I am stuck a bit here. I need to monkeypatch some peewee/psycopg2 library functionality to run test_db queries with pytest. Other wise i have performed sanity testing.



Note: To make it bit interesting for me, i am using peewee, marshmallow and falcon for the first time.

***My article: *** [Falcon on Alibaba's Cloud](https://alibabacloud.com/blog/building-very-fast-app-backends-with-falcon-web-framework-on-pypy_594282) 
