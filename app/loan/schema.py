from marshmallow import Schema, fields, validate
from app.settings import MIN_APPROVED_AMOUNT
from app.settings import MIN_REQUESTED_AMOUNT

class LoanSchema(Schema):
    approved_amount = fields.Float(validate=[validate.Range(min=MIN_APPROVED_AMOUNT,error="Invalid approved amount")])

class LoanComponentSchema(Schema):
    requested_amount = fields.Float(required=True,validate=[validate.Range(min=MIN_REQUESTED_AMOUNT,error="Minimum requested amount required")])
    interest_rate = fields.Float(required=True,validate=[validate.Range(min=0,max=100,error="Minimum interest rate required")])
    process_fee = fields.Float(validate=[validate.Range(min=0,error="Process fee must be positive")])
    duration = fields.Integer(required=True,validate=[validate.Range(min=1)])
    interest_type = fields.Integer(validate=[validate.Range(min=1,max=2)])
    modified = fields.DateTime(required=False)
    created = fields.DateTime(required=False)
    id = fields.Integer(required=False)