import jwt, falcon

from app.settings import SECRET
from app.accounts.models import Role, User
from app.accounts.schema import UserSchema

class Authorize(object):
    '''
    Check for logged in user, allow for agent and admin. For self, check for self_id
    self_id rule will globally prevent IDOR attacks as well
    '''
    def __init__(self,roles):
        self._roles= roles or [Role.CUSTOMER]

    def __call__(self, req, resp, resource, params):
        token = req.get_header('Authorization')
        is_token_valid, data = self._token_valid_and_data(token)
        print(data)
        if not is_token_valid:
            raise falcon.HTTPUnauthorized("Invalid token",data,None)
        elif (Role.SELF in self._roles) and (int(params.get("self_id",-1))!=int(data["id"])):
            raise falcon.HTTPUnauthorized("Invalid token","You are authorized for your data only",None)
        elif (data["role"] not in self._roles) and (Role.SELF not in self._roles):
            raise falcon.HTTPUnauthorized("Invalid token","You are not authorized with your current role",None)
        else:
            pass

        user = User.get(User.id == int(data["id"]))
        if str(user.secret) != str(data["uid"]):
            raise falcon.HTTPUnauthorized("Invalid token","Token changed for your account, please login again",None)
        req.context["me"] = user

    def _token_valid_and_data(self,token):
        try:
            data=jwt.decode(token,SECRET, algorithm='HS256')
            return (True,data)
        except jwt.exceptions.ExpiredSignatureError as e:
            return (False,"Token expired")
        except Exception as e:
            return (False, "Please provide a valid token.")





