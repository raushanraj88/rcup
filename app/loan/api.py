import falcon, datetime

from app.settings import PAGINATION_LIMIT
from app.utils.auth_utility import Authorize
from app.utils.loan_utility import loan_exist
from app.accounts.models import Role
from app.loan.schema import LoanComponentSchema, LoanSchema
from app.loan.models import LoanComponent, Loan
from app.accounts.models import User
from app.database import db


class LoanApi(object):
    @falcon.before(loan_exist)
    @falcon.before(Authorize([Role.CUSTOMER, Role.AGENT, Role.ADMIN]))
    def on_get(self,req,resp,loan_id):
        me = req.context["me"]
        loan = req.context["loan"]
        if me.role.id == Role.CUSTOMER and loan.user!=me:
            raise falcon.HTTPUnauthorized("Access  Denied","You can view your own loan details only",None)
        loan_component = loan.get_recent_component()
        history_component = loan.get_history_component()
        resp.media ={"data": loan_component,"history":history_component}
        resp.status = falcon.HTTP_200

    @falcon.before(loan_exist)
    @falcon.before(Authorize([Role.AGENT, Role.ADMIN]))
    def on_post(self,req,resp,loan_id):
        """
        :param req:
        :param resp:
        :param loan_component_id:
        :return:  Edit loan by creating new loan_component_id. 500 error not handled as its not a problem now.
        """
        data = req.media
        loan = req.context["loan"]
        if loan.state > Loan.CREATED:
            raise falcon.HTTPUnauthorized("Access  Denied", "Sorry, you can't edit approved/rejected loan", None)
        loan_component, lc_errors = LoanComponentSchema(only=("requested_amount","interest_rate","duration",)).load(data)
        if lc_errors:
            resp.media = {"errors": lc_errors}
            resp.status = falcon.HTTP_400
            return
        loan_component["loan_id"]=loan_id
        LoanComponent.create(**loan_component)
        resp.media = {"message": "Loan edited successfully"}
        resp.status = falcon.HTTP_200
        return

@falcon.before(Authorize([Role.AGENT]))
class LoanRequest(object):
    def on_post(self,req,resp):
        """
        :param req:
        :param resp:
        :return: It will create loan object by agent for a customer user.
        """
        me = req.context["me"]
        data = req.media
        loan_meta = {"user":data.pop("user")}
        loan_component = data
        loan_meta_data, lm_errors = LoanSchema().load(loan_meta)
        loan_component, lc_errors = LoanComponentSchema().load(loan_component)
        if lc_errors or lm_errors:
            resp.media = {"errors":["Bad data received"]}
            resp.status = falcon.HTTP_400
            return

        with db.atomic() as transaction:
            try:
                CUSTOMER_ROLE = Role.get(Role.id == Role.CUSTOMER)
                user = User.get((User.id == int(loan_meta['user'])) & (User.role == CUSTOMER_ROLE))
                loan = Loan.create(user = user,created_by=me)
                loan_component["loan_id"]=loan.id
                component = LoanComponent.create(**loan_component)
                print(loan.id)
                resp.media = {"message": "Loan requested succesfully"}
                resp.status = falcon.HTTP_200
                return
            except (User.DoesNotExist,Exception) as e:
                print(str(e))
                transaction.rollback()
                resp.media = {"errors": ["unexpected issue"]}
                resp.status = falcon.HTTP_400
                return


@falcon.before(Authorize([Role.ADMIN,Role.AGENT,Role.CUSTOMER]))
class LoanList(object):
    def on_get(self,req,resp):
        """
        :param req:
        :param resp:
        :return: Filter on the basis of req params, created and modified date format are YYYY-MM-DD
        """
        me = req.context["me"]
        loan_data=[]
        state = req.params.get("state",None)
        created_date = req.params.get("created",None)
        modified = req.params.get("modified",None)
        page = req.params.get("page",1)
        limit = min(req.params.get("limit", 20), PAGINATION_LIMIT)
        loans = Loan.select()
        if me.role.id == Role.CUSTOMER:
            loans=loans.where(Loan.user == me)
        if state:
            loans = loans.where(Loan.state == int(state))
        if created_date:
            created = datetime.datetime.strptime(created_date,"%Y-%m-%d")
            loans = loans.where((Loan.created > created) & (Loan.created < created + datetime.timedelta(days=1)))
        if modified:
            modified = datetime.datetime.strptime(modified,"%Y-%m-%d")
            loans = loans.where((Loan.modified > modified) & (Loan.modified < modified + datetime.timedelta(days=1)))

        loans = loans.paginate(page,limit).execute()
        for  loan in loans:
            loan_data.append(loan.get_recent_component())

        resp.media = loan_data
        resp.status = falcon.HTTP_200
        return


@falcon.before(loan_exist)
@falcon.before(Authorize([Role.ADMIN]))
class LoanApprove(object):
    def on_post(self,req,resp,loan_id):
        """
        :param req:
        :param resp:
        :param loan_id:
        :return: Loan approved by admin only with approved amount
        """
        loan = req.context["loan"]
        data = req.media
        state = data.get("state")
        amount = data.get("approved_amount",0)
        if loan.state in [Loan.REJECTED,Loan.APPROVED] or state not in [Loan.REJECTED,Loan.APPROVED]:
            raise falcon.HTTPUnauthorized("Unsupported","Can't change already rejected or approved loan")
        loan.state = int(state)
        loan.approved_amount = amount if amount > 0 else loan.get_recent_component()["requested_amount"]
        loan.save()
        resp.media = {"message":"loan is updated"}
        resp.status = falcon.HTTP_200
        return

