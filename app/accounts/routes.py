from app.accounts.auth import Register, Login
from app.accounts.users import UserListApi, UserProfileApi, AgentCreateApi

urls = ([('/accounts/register',Register()),{}],
        [('/accounts/login',Login()),{}],
        [('/user/profile/{self_id}',UserProfileApi()),{}],
        [('/users/',UserListApi()),{}],
        [('/user/agent/create',AgentCreateApi()),{}],
        )