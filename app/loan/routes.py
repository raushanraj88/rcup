from app.loan.api import LoanApi, LoanApprove, LoanList, LoanRequest
urls = (
        [('/loan/request',LoanRequest()),{}],
        [('/loan/{loan_id}',LoanApi()),{}],
        [('/loan/approve/{loan_id}',LoanApprove()),{}],
        [('/loan/list',LoanList()),{}],
        )