import sys, os
sys.path.append(os.path.abspath('..'))

from app.database import db
from app.accounts.models import User, Role, PersonalInfo, FinancialInfo
from app.loan.models import LoanComponent, Loan


class DatabaseOperation(object):
    def __init__(self,db):
        self.db = db
        self.tables = [User, Role, PersonalInfo, FinancialInfo, Loan, LoanComponent]

    def create_tables(self):
        self.db.create_tables(self.tables)

    def drop_tables(self):
        self.db.drop_tables(self.tables)

    def init_role_tables(self):
        for role_id in (1, 2, 3):
            Role.create(id=role_id)


if __name__  == "__main__":
    dbops = DatabaseOperation(db)
    dbops.create_tables()
    #dbops.drop_tables()
    try:
        dbops.init_role_tables()
    except Exception as e:
        print("UniqueViolation error",str(e))