import datetime, uuid, jwt

from peewee import *
from werkzeug.security import generate_password_hash, check_password_hash

from app.settings import SECRET, TOKEN_EXPIRATION_TIME
from app.database.models import BaseModel


class Role(BaseModel):
    '''
    User roles are customer, agent, admin. In future there may be many to many relation.
    '''
    SELF = 0
    CUSTOMER = 1
    AGENT = 2
    ADMIN = 3

    ROLE_CHOICES = (
        (CUSTOMER, 'customer'),
        (AGENT, 'agent'),
        (ADMIN, 'admin'),
    )
    id = IntegerField(choices=ROLE_CHOICES,default=1,unique=True)

    class Meta:
        db_table ="rcup_role"

class User(BaseModel):
    '''
    User model represents the actor of the system. It can be admin, agent or customer or may have multiple roles
    '''
    id = AutoField()
    name = CharField(max_length=255,null=True)
    username = CharField(unique=True,index=True,max_length=255)
    password = TextField()
    email = CharField(unique=True,index=True,max_length=255)
    created = DateTimeField(default=datetime.datetime.utcnow)
    modified = DateTimeField()
    role = ForeignKeyField(Role, backref="users")
    email_verified = BooleanField(default=False)
    is_active = BooleanField(default=True)
    secret = UUIDField(default=uuid.uuid4)
    class Meta:
        db_table="rcup_user"
        indexes = (
            (('username','is_active'),False),
        )


    def generate_token(self,expiration=TOKEN_EXPIRATION_TIME):
        return jwt.encode({'exp':datetime.datetime.utcnow()+datetime.timedelta(seconds=expiration),
                           'username':self.username,'uid':str(self.secret),'role':self.role.id,'id':self.id},SECRET,algorithm='HS256')



    @staticmethod
    def hash_password(password):
        """
        :param password:
        :return: sha256 hashed password
        """
        return generate_password_hash(password)

    @staticmethod
    def check_hash(hash,password):
        return check_password_hash(hash,password)

    def invalidate_token(self):
        self.secret = uuid.uuid4()



    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.now()
        return super(User, self).save(*args, **kwargs)


class PersonalInfo(BaseModel):
    '''
    No relation with user table, In case of scaling we can move this data from central database with no integrity problem.
    '''
    user_id = IntegerField()
    dob = DateTimeField(null=True)
    address = TextField(null=True)
    phone_number = CharField(null=True)


class FinancialInfo(BaseModel):
    '''
    Flat structure to move this informations into different dbs.
    '''
    user_id = IntegerField(unique=True)
    credit_score = DecimalField(default=float('-inf'))
    occupation = TextField(null=True)