from app.tests.db import db
from app.database.db_ops import DatabaseOperation

def test_create_table():
    DatabaseOperation(db).create_tables()
    assert True

def test_create_role():
    try:
        DatabaseOperation(db).init_role_tables()
        print("Roles created ====> Success.")
    except Exception as e:
        print(str(e))
        print("Roles already existed ====> Sucesss.")
    assert True
