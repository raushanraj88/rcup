import json, falcon, psycopg2, peewee

from app.database import db
from app.accounts.models import User, Role
from app.accounts.schema import UserSchema
from app.accounts.exceptions import UsernamePasswordNotMatch


class Register(object):

    def on_post(self,req,resp):
        '''
        :param req:
        :param resp:
        :return: jwt_token
        This view will receive username, email, password to signup user into system,password get hashed in schema only
        '''
        data = req.media
        data,errors=UserSchema().load(data)
        if errors:
            resp.media =  {"errors":errors}
            resp.status = falcon.HTTP_400
            return
        role = Role.get(Role.id==Role.CUSTOMER)
        with db.atomic() as transaction:
            try:
                user = User.create(username=data["username"],email=data["email"],password=data["password"]
                                   ,secret=data["secret"],role=role)
                token = user.generate_token()
                resp.media = {"token":token.decode("utf-8")}
                resp.status = falcon.HTTP_200
                return
            except psycopg2.errors.UniqueViolation as e:
                transaction.rollback()
                resp.media = {"errors":["username and email needs to be unique"]}
                resp.status = falcon.HTTP_400
                return

class Login(object):

    def on_post(self,req,resp):
        """
        :param req:
        :param resp:
        :return: jwt_token
        Username validation is only to prevent extra call on db in case of malicious or very long payload.
        """
        data = req.media
        username = data.get("username")
        password = data.get("password")
        errors = UserSchema(only=("username",)).validate(data)
        if errors:
            resp.media =  {"errors":errors}
            resp.status = falcon.HTTP_400
            return
        with db.atomic() as transaction:
            try:
                user = User.get(User.username == username)
                if User.check_hash(user.password,password):
                    token = user.generate_token()
                    resp.media = {"token": token.decode("utf-8")}
                    resp.status = falcon.HTTP_200
                    return
                else:
                    raise UsernamePasswordNotMatch
            except (User.DoesNotExist,UsernamePasswordNotMatch)as e:
                transaction.rollback()
                resp.media = {"errors": ["username and password not matched"]}
                resp.status = falcon.HTTP_400
                return













