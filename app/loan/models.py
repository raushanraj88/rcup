import datetime

from peewee import *
from app.database.models import BaseModel
from app.accounts.models import  User
from app.loan.schema import LoanComponentSchema



class Loan(BaseModel):
    """
    Storing meta info of loan parent model. These info can't be edited further.
    """
    CREATED = 1
    APPROVED = 2
    REJECTED = 3

    STATE_CHOICES = (
        (CREATED, 'created'),
        (APPROVED, 'approved'),
        (REJECTED, 'rejected'),
    )
    id = AutoField()
    user = ForeignKeyField(User, backref = "loans")
    state = IntegerField(choices=STATE_CHOICES,default=1)
    approved_amount = FloatField(default=0)
    approved_by = ForeignKeyField(User, backref="approved",null=True)
    created_by = ForeignKeyField(User, backref = "loans_created")
    rejection_comment = TextField(default='')
    created = DateTimeField(default=datetime.datetime.utcnow)
    modified = DateTimeField()

    class Meta:
        db_table = "rcup_loan"


    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.utcnow()
        return super(Loan, self).save(*args, **kwargs)

    def get_recent_component(self):
        """
        :return: It will return the recent changes in the loan component values
        """
        component = LoanComponent.select().where((self.id == LoanComponent.loan_id) & (LoanComponent.is_discarded == False))\
            .order_by(LoanComponent.created.desc()).get()
        data,errors = LoanComponentSchema().dump(component)
        data["loan_id"] = self.id
        data["user"] = self.user.id
        data["emi"] = component.emi(self.approved_amount)
        data["approved_amount"] = self.approved_amount
        data["state"] = self.state
        return data

    def get_history_component(self):
        component = LoanComponent.select().where(
            (self.id == LoanComponent.loan_id) & (LoanComponent.is_discarded == False)) \
            .order_by(LoanComponent.created.desc())
        data, errors = LoanComponentSchema(many=True).dump(component)
        return data




class LoanComponent(BaseModel):
    """
    This table is basically for tracking changes in loan requests by the agent, no foreign relation is established. As it
    is growing table, it can be decoupled later to other secondary database easily.
    """
    FLAT =1
    REDUCING=2

    INTEREST_TYPE = (
        (FLAT,"flat"),
        (REDUCING,"reducing")
    )
    id = AutoField()
    #loan = ForeignKeyField(Loan,backref="components")
    loan_id = IntegerField()
    requested_amount = FloatField()
    duration = IntegerField()
    #duration is the number of months
    process_fee = IntegerField(default=0)
    interest_rate = FloatField(default=1)
    interest_type = CharField(choices=INTEREST_TYPE,default=2)
    is_discarded = BooleanField(default=False)
    created = DateTimeField(default=datetime.datetime.now)
    modified = DateTimeField()


    def calculate_flat_emi(self,approved_amount):
        rate = self.interest_rate/ (12 * 100)  # one month interest
        time =int(self.duration)
        principle = approved_amount or self.requested_amount
        emi = (principle * rate * pow(1 + rate, time)) / (pow(1 + rate, time) - 1)
        return emi

    def calculate_reducing_emi(self,approved_amount):
        principle = approved_amount or self.requested_amount
        time = int(self.duration)
        rate = self.interest_rate
        emi = (principle*(1+rate/100))/time
        return emi

    def emi(self,approved_amount):
        if self.interest_type == LoanComponent.FLAT:
            return self.calculate_flat_emi(approved_amount)
        return self.calculate_reducing_emi(approved_amount)

    def save(self, *args, **kwargs):
        self.modified = datetime.datetime.utcnow()
        return super(LoanComponent, self).save(*args, **kwargs)


