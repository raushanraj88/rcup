FROM python:3.6

LABEL maintainer "Raushan Raj <shemnt.raj38@gmail.com>"

RUN apt-get update

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

