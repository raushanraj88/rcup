


class PeeweeConnectionMiddleware(object):
    def __init__(self,database):
        self.database = database

    def process_request(self, req, resp):
        self.database.connect()

    def process_response(self, req, resp, resource):
        if not self.database.is_closed():
            self.database.close()