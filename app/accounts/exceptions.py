

class UsernamePasswordNotMatch(Exception):
    def __init__(self,*args,**kwargs):
        super(UsernamePasswordNotMatch,self).__init__(*args,**kwargs)
        self.errors = "Username and Password didn't matched"

class AuthorizationPriorityError(Exception):
    def __init__(self,*args,**kwargs):
        super(AuthorizationPriorityError,self).__init__(*args,**kwargs)
        self.errors = "User with greater priority role can perform action on lower one."

class MarshalLoadError(Exception):
    pass