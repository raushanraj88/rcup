import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

class FTestingDB(object):
    def __init__(self):
        self.conn = psycopg2.connect(dbname='postgres',
                           user='rcup', host='postgres',
                           password='rcup')
    def create_db(self):
        try:
            print("creating test db")
            con = self.conn
            con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            cur = con.cursor()
            db_name = 'rcup_test'
            cur.execute("CREATE DATABASE %s  ;" %db_name )
        except psycopg2.errors.DuplicateDatabase as e:
            pass

    def drop_db(self):
        try:
            con = self.conn
            con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            cur = con.cursor()
            db_name = 'rcup_test'
            cur.execute("DROP DATABASE IF EXISTS %s  ;" % db_name)
            print("droping test db")
        except Exception as e:
            print(str(e))
