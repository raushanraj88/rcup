
from app.database import db

from app.falcon_config import FalconConfig

config = FalconConfig(db)
config.create_app()
app = application = config.app

