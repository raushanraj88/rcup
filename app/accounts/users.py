import falcon, json, psycopg2

from app.settings import PAGINATION_LIMIT
from app.utils.auth_utility import Authorize
from app.accounts.models import  User, Role
from app.accounts.schema import UserSchema
from app.accounts.exceptions import MarshalLoadError, AuthorizationPriorityError


@falcon.before(Authorize([Role.SELF,Role.AGENT,Role.ADMIN]))
class UserProfileApi(object):

    def on_get(self,req,resp,self_id):
        try:
            user = User.get(User.id == int(self_id))
            user_data,errors=UserSchema().dump(user)
        except User.DoesNotExist as e:
            resp.media = {"errors":["No user found"]}
            resp.status = falcon.HTTP_200
            return
        resp.media = {"data":user_data}
        resp.status = falcon.HTTP_200

    def on_post(self,req,resp,self_id):
        """
        :param req:
        :param resp:
        :return:
        Profile can be edited by customer, admin, agent
        """
        data = req.media
        me  = req.context["me"]
        errors = []
        try:
            user = User.get(User.id == int(self_id))
            if me.role.id < user.role.id:
                raise AuthorizationPriorityError
            user_data, error = UserSchema(only=('name','username','email','password','is_active')).load(data)
            if error:
                raise MarshalLoadError("Error loading user")
            user.update(**user_data).execute()
        except (User.DoesNotExist) as e:
            errors.append("User not found")
        except MarshalLoadError as e:
            errors.append("Bad Data")
        except AuthorizationPriorityError as e:
            errors.append(str(e))
        except psycopg2.errors.UniqueViolation as e:
            errors.append("Username and email must be unique")
        finally:
            if errors:
                resp.media = {"errors": errors}
                resp.status = falcon.HTTP_400
                return


@falcon.before(Authorize([Role.AGENT,Role.ADMIN]))
class UserListApi(object):
    """
    Agents and admin can list users, depending on current role they can access only for lower roles.
    """
    def on_get(self,req,resp):
        me= req.context["me"]
        limit = min(req.params.get("limit",20),PAGINATION_LIMIT)
        page = req.params.get("page",1)
        users = User.select().join(Role).where((Role.id < me.role.id) | (Role.id == me.role)).order_by(User.created).paginate(page,limit).execute()
        user_list, errors = UserSchema(only=("id","username","email","is_active","role"),many=True).dump(users)
        for user in user_list:
            user["role"]=user["role"].id
        resp.media = {"data":user_list,"page":page}
        resp.status = falcon.HTTP_200


@falcon.before(Authorize([Role.ADMIN]))
class AgentCreateApi(object):
    """
    Admin can create any user as a agent, changing secret will invalidate all other tokens in subsequent calls.
    """
    def on_post(self,req,resp):
        data = req.media
        user_id = data["user_id"]
        try:
            user = User.get(User.id == int(user_id))
            user.role = Role.get(Role.id == Role.AGENT)
            user.invalidate_token()
            user.save()
            resp.media = {"message":"agent created"}
            resp.status = falcon.HTTP_200
        except User.DoesNotExist as e:
            resp.media = {"errors":["User not found"]}
            resp.status = falcon.HTTP_400
            return

